const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let UserSchema = new Schema({
  username: {
    type: String,
    max: 100,
    required: [true, "El nombre del usuario es necesario"],
  },
  email: {
    type: String,
    max: 150,
    unique: true,
    required: [true, "El precio del usuario es necesario"],
  },
  password: {
    type: String,
    required: [true, "la imagen del usuario es necesario"],
  },
});

module.exports = mongoose.model("User", UserSchema);
