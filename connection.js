// Mongoose connection
const mongoose = require("mongoose");
const dev_db_url = "mongodb://localhost:27017/blogDB";
const production_db_url =
  "mongodb+srv://franka107:ggej9db6@cluster0.zioq8.mongodb.net/testDB ";
const mongoDB = process.env.MONGODB_URI || production_db_url;
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connnection error:"));
//-----------------------

module.exports = mongoose.connection;
